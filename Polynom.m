b = [];
c = [];
d = []; 
x = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6];
Xn = sum(x);
fprintf('Xn = %.1f\n', Xn);

% find X2n
for x = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6];
    x2 = x.^2;
    b = [b , x2];
end

X2n = sum(b);
fprintf('X2n = %.2f\n', X2n);

% find X3n
for x = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6];
    x3 = x.^3;
    c = [c , x3];
end
X3n = sum(c);
fprintf('X3n = %.3f\n', X3n);

% findX4n 

for x = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6];
    x4 = x.^4;
    d = [d , x4];
end
X4n = sum(d);
fprintf('X4n = %.4f\n', X4n);
%find Yn

x = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6];
y = [1, 2.5, 3, 4.5, 5, 5.5, 6, 6.5];
Y_array= sum(y);
fprintf('Yn = %.f\n', Y_array);

% find XnYn
XnYn_array = x .*y;
XnYn = sum(XnYn_array);
fprintf('XnYn = %.f\n', XnYn);

% find X2Yn
X2nYn_array = b .*y;
X2nYn = sum(X2nYn_array);
fprintf('X2Yn = %.2f', X2nYn);

% matrix
N = 8;
x = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6];
y = [1, 2.5, 3, 4.5, 5, 5.5, 6, 6.5];
delta = [N Xn X2n; Xn X2n X3n; X2n X3n X4n];
delta_one = [Y_array Xn X2n; XnYn X2n X3n; X2nYn X3n X4n];
delta_two = [N Y_array X2n; Xn XnYn X3n; X2n X2nYn X4n];
delta_three = [N Xn Y_array; Xn X2n XnYn; X2n X3n X2nYn];

det_delta = det(delta);
det_delta_one = det(delta_one);
det_delta_two = det(delta_two);
det_delta_three = det(delta_three);
fprintf('Delta_determinant = %.4f\n', det_delta);
fprintf('Delta_one_determinant = %.4f\n', det_delta_one);
fprintf('Delta_second_determinant = %.4f\n', det_delta_two);
fprintf('Delta_three_determinant = %.4f\n', det_delta_three);

coefficient_a = det_delta_one / det_delta;
coefficient_b = det_delta_two / det_delta;
coefficient_c = det_delta_three / det_delta;
fprintf('P(x) = %.4f%+.4fx%.4fx^2\n', coefficient_a, coefficient_b,  coefficient_c);

p = polyfit(x, y, 2);
disp(p);

x = 0.2:1.6; 
y = -0.2500+7.0238*x-1.7857*x.^2;
xi = 0.2:.01:1.6;
yi = interp1(x, y, xi);

plot(x, y, 'O', xi, yi);
title('Combine Plots')

hold on

plot([0.2 0.4 0.6 0.8 1.0 1.2 1.4 1.6],[1 2.5 3 4.5 5 5.5 6 6.5]);
hold off
