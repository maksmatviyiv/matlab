delta = [0.8 0.2 0 0 0 0; 0.2 0.8 0.2 0 0 0; 0 0.2 0.8 0.2 0 0;
         0 0 0.2 0.8 0.2 0; 0 0 0 0.2 0.8 0.2; 0 0 0 0 0.2 0.8];
delta_determinant = det(delta);
fprintf("Delta determinant = %.4f\n", delta_determinant);

delta_second = [-1.2 0.2 0 0 0 0; 1.2 0.8 0.2 0 0 0; -1.2 0.2 0.8 0.2 0 0;
                0 0 0.2 0.8 0.2 0; 0 0 0 0.2 0.8 0.2; 0 0 0 0 0.2 0.8];
 delta_second_determinant = det(delta_second);
 fprintf("Delta second determinant = %.4f\n", delta_second_determinant);
 
 delta_three = [0.8 -1.2 0 0 0 0; 0.2 1.2 0.2 0 0 0; 0 -1.2 0.8 0.2 0 0;
                0 0 0.2 0.8 0.2 0; 0 0 0 0.2 0.8 0.2; 0 0 0 0 0.2 0.8];
 delta_three_determinant = det(delta_three);
 fprintf("Delta three determinant = %.4f\n", delta_three_determinant); 
 
 delta_four = [0.8 0.2 -1.2 0 0 0; 0.2 0.8 1.2 0 0 0; 0 0.2 -1.2 0.2 0 0;
               0 0 0 0.8 0.2 0; 0 0 0 0.2 0.8 0.2; 0 0 0 0 0.2 0.8];
 delta_four_determinant = det(delta_four);
 fprintf("Delta four determinant = %.4f\n", delta_four_determinant);
 
 delta_five = [0.8 0.2 0 -1.2 0 0; 0.2 0.8 0.2 1.2 0 0; 0 0.2 0.8 -1.2 0 0;
               0 0 0.2 0 0.2 0; 0 0 0 0 0.8 0.2; 0 0 0 0 0.2 0.8];
 delta_five_determinant = det(delta_five);
 fprintf("Delta five determinant = %.4f\n", delta_five_determinant);
 
 delta_six = [0.8 0.2 0 0 -1.2 0; 0.2 0.8 0.2 0 1.2 0; 0 0.2 0.8 0.2 -1.2 0;
              0 0 0.2 0.8 0 0; 0 0 0 0.2 0 0.2; 0 0 0 0 0 0.8];
 delta_six_determinant = det(delta_six);
 fprintf("Delta six determinant = %.4f\n", delta_six_determinant);
 
 delta_seven = [0.8 0.2 0 0 0 -1.2; 0.2 0.8 0.2 0 0 1.2; 0 0.2 0.8 0.2 0 -1.2;
                0 0 0.2 0.8 0.2 0; 0 0 0 0.2 0.8 0; 0 0 0 0 0.2 0];
 delta_seven_determinant = det(delta_seven);
 fprintf("Delta seven determinant = %.4f\n", delta_seven_determinant);
 
 derivative_y_two = delta_second_determinant / delta_determinant;
 fprintf("Derivative y two = %.4f\n", derivative_y_two);
 
 deravative_y_three = delta_three_determinant / delta_determinant;
 fprintf("Derivative y three = %.4f\n", deravative_y_three);
 
 deravative_y_four = delta_four_determinant / delta_determinant;
 fprintf("Derivative y four = %.4f\n", deravative_y_four);
 
 deravative_y_five = delta_five_determinant / delta_determinant;
 fprintf("Derivative y five = %.4f\n", deravative_y_five);
 
 deravative_y_six = delta_six_determinant / delta_determinant;
 fprintf("Derivative y six = %.4f\n", deravative_y_six);
 
 deravative_y_seven = delta_seven_determinant / delta_determinant;
 fprintf("Derivative y seven = %.4f\n", deravative_y_seven);
hn = 0.2;
x = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6];
y = [1, 2.5, 3, 4.5, 5, 5.5, 6, 6.5];
%coefficient for n = 1;
a = y(1);
b = 1/hn * (y(2) - y(1)) - (1/6 * hn *(derivative_y_two + 2 *0));
c = 0;
d = 1/6 * hn * (derivative_y_two - 0);
fprintf("P1(x) = %.f+%.4f(x-0.2)+%.f(x-0.2)^2%.4f(x-0.2)^3\n", a, b, c, d);
%coefficient for n = 2;
a1 = y(2);
b1 = 1/hn * (y(3)-y(2)) - (1/6 * hn * (deravative_y_three + 2 * derivative_y_two));
c1 = 1/2 * derivative_y_two;
d1 = 1/6*hn*(deravative_y_three - derivative_y_two);
fprintf("P2(x) = %.1f+%.4f(x-0.4)%.4f(x-0.4)^2+%.4f(x-0.4)^3\n", a1, b1, c1, d1);

%coefficient for n = 3;
a2 = y(3);
b2 = 1/hn * (y(4)-y(3)) - (1/6 * hn * (deravative_y_four + 2 * deravative_y_three));
c2 = 1/2 * deravative_y_three;
d2 = 1/6*hn*(deravative_y_four - deravative_y_three);
fprintf("P3(x) = %.1f+%.4f(x-0.6)+%.4f(x-0.6)^2%.4f(x-0.6)^3\n", a2, b2, c2, d2);

%coefficient for n = 4;
a3 = y(4);
b3 = 1/hn * (y(5)-y(4)) - (1/6 * hn * (deravative_y_five + 2 * deravative_y_four));
c3 = 1/2 * deravative_y_four;
d3 = 1/6*hn*(deravative_y_five - deravative_y_four);
fprintf("P4(x) = %.1f+%.4f(x-0.8)%.4f(x-0.8)^2+%.4f(x-0.8)^3\n", a3, b3, c3, d3);

%coefficient for n = 5;
a4 = y(5);
b4 = 1/hn * (y(6)-y(5)) - (1/6 * hn * (deravative_y_six + 2 * deravative_y_five));
c4 = 1/2 * deravative_y_five;
d4 = 1/6*hn*(deravative_y_six - deravative_y_five);
fprintf("P5(x) = %.1f+%.4f(x-1)+%.4f(x-1)^2%.4f(x-1)^3\n", a4, b4, c4, d4);


%coefficient for n = 6;
a5 = y(6);
b5 = 1/hn * (y(7)-y(6)) - (1/6 * hn * (deravative_y_seven + 2 * deravative_y_six));
c5 = 1/2 * deravative_y_six;
d5 = 1/6*hn*(deravative_y_seven - deravative_y_six);
fprintf("P6(x) = %.1f+%.4f(x-1.2)%.4f(x-1.2)^2+%.4f(x-1.2)^3\n", a5, b5, c5, d5);

%coefficient for n = 7;
a6 = y(7);
b6 = 1/hn * (y(8)-y(7)) - (1/6 * hn * (0 + 2 * deravative_y_seven));
c6 = 1/2 * deravative_y_seven;
d6 = 1/6*hn*(0 - deravative_y_seven);
fprintf("P7(x) = %.1f+%.4f(x-1.4)+%.4f(x-1.4)^2%.4f(x-1.4)^3\n", a6, b6, c6, d6);

x = 0.2:.01:0.4;
y = 1+7.5718*(x-0.2)-0.0718*(x-0.2).^3;
plot(x, y);
hold on 
x1 = 0.4:.01:0.6;
y1 = 2.5+2.5564*(x1-0.4)-1.0769*(x1-0.4).^2+0.1590*(x1-0.4).^3;
plot(x1, y1);
x2 = 0.6:.01:0.8;
y2 = 3.0+7.4026*(x2-0.6)+1.3078*(x2-0.6).^2-0.1641*(x2-0.6).^3;
plot(x2, y2);
x3 = 0.8:.01:1.0;
y3 = 4.5+2.6333*(x3-0.8)-1.1542*(x3-0.8).^2+0.0976*(x3-0.8).^3;
plot(x3, y3);
x4 = 1.0:.01:1.2;
y4 = 5.0+2.4643*(x4-1)+0.3092*(x4-1).^2-0.0261*(x4-1).^3;
plot(x4, y4);
x5 = 1.2:.01:1.4;
y5 = 5.5+2.5096*(x5-1.2)-0.0824*(x5-1.2).^2+0.0069*(x5-1.2).^3;
plot(x5, y5);
x6 = 1.4:.01:1.6;
y6 = 6.0+2.4973*(x6-1.4)+0.0206*(x6-1.4).^2-0.0014*(x6-1.4).^3;
plot(x6, y6);
hold off